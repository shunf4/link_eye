package kuesji.link_eye;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.*;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

public class LinkHandler extends Activity {

	private EditText urlArea;
	private CheckBox actionSave;
	private Button actionCopy,actionOpen,actionShare;
	private LinearLayout contentArea;
	private String currentAction = "";
	private SharedPreferences sharedPreferences;
	private Set<String> ruleBrowserLabelToHideSet;
	private Set<String> ruleShareLabelToHideSet;
	private Set<String> ruleBrowserLabelOnlyAllowSet;
	private Set<String> ruleShareLabelOnlyAllowSet;
	private Map<String, Integer> ruleBrowserLabelOrderMap;
	private Map<String, Integer> ruleShareLabelOrderMap;

	private int itemHeight = 160;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		sharedPreferences = getSharedPreferences("main", Context.MODE_PRIVATE);
		String rulesStr = sharedPreferences.getString("rules", "");
		ruleBrowserLabelToHideSet = new HashSet<>();
		ruleShareLabelToHideSet = new HashSet<>();
		ruleBrowserLabelOnlyAllowSet = new HashSet<>();
		ruleShareLabelOnlyAllowSet = new HashSet<>();
		ruleBrowserLabelOrderMap = new HashMap<>();
		ruleShareLabelOrderMap = new HashMap<>();


		for (String oneRuleStr : rulesStr.split("\n")) {
			oneRuleStr = oneRuleStr.trim();
			if (TextUtils.isEmpty(oneRuleStr)) {
				continue;
			}
			String ruleName = oneRuleStr.split(":", 2)[0];
			ruleName = ruleName.trim();
			String[] rule;
			try {
				switch (ruleName) {
					case "hideBrowser":
						rule = oneRuleStr.split(":", 2);
						ruleBrowserLabelToHideSet.add(rule[1]);
						break;
					case "hideShare":
						rule = oneRuleStr.split(":", 2);
						ruleShareLabelToHideSet.add(rule[1]);
						break;
					case "onlyAllowBrowser":
						rule = oneRuleStr.split(":", 2);
						ruleBrowserLabelOnlyAllowSet.add(rule[1]);
						break;
					case "onlyAllowShare":
						rule = oneRuleStr.split(":", 2);
						ruleShareLabelOnlyAllowSet.add(rule[1]);
						break;
					case "setBrowserOrder":
						rule = oneRuleStr.split(":", 3);
						ruleBrowserLabelOrderMap.put(rule[2], Integer.parseInt(rule[1].trim()));
						break;
					case "setShareOrder":
						rule = oneRuleStr.split(":", 3);
						ruleShareLabelOrderMap.put(rule[2], Integer.parseInt(rule[1].trim()));
						break;
					case "itemHeight":
						rule = oneRuleStr.split(":", 2);
						try {
							itemHeight = Integer.parseInt(rule[1]);
						} catch (Exception e) {
							e.printStackTrace();
						}
						break;
					default:
						continue;
				}
			} catch (NumberFormatException e) {
				continue;
			}
 		}

		getWindow().setStatusBarColor(Theme.background_primary);
		getWindow().setNavigationBarColor(Theme.background_primary);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
			setTaskDescription(new ActivityManager.TaskDescription(getString(R.string.app_name),R.drawable.ic_link_eye,getColor(R.color.background_primary)));
		} else {
			setTaskDescription(new ActivityManager.TaskDescription(getString(R.string.app_name)));
		}


		setContentView(R.layout.link_handler);
		urlArea = findViewById(R.id.link_handler_url);
		urlArea.addTextChangedListener(new TextWatcher() {
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				listHandlers(currentAction.equals("") ? "open" : currentAction);
			}
			public void afterTextChanged(Editable s) {}
		});
		actionSave = findViewById(R.id.link_handler_save_check);
		actionCopy = findViewById(R.id.link_handler_action_copy);
		actionCopy.setOnClickListener((v)->{
			ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
			clipboard.setPrimaryClip(ClipData.newPlainText("link eye", urlArea.getText().toString()));
			Toast.makeText(this,getString(R.string.link_handler_copied_to_clipboard), Toast.LENGTH_SHORT).show();
		});
		actionOpen = findViewById(R.id.link_handler_action_open);
		actionOpen.setOnClickListener((v)->{
			actionOpen.setBackgroundColor(getColor(R.color.background_secondary));
			actionShare.setBackgroundColor(getColor(R.color.background_seconday_not_selected));

			listHandlers("open");
		});
		actionShare = findViewById(R.id.link_handler_action_share);
		actionShare.setOnClickListener((v)->{
			actionShare.setBackgroundColor(getColor(R.color.background_secondary));
			actionOpen.setBackgroundColor(getColor(R.color.background_seconday_not_selected));

			listHandlers("share");
		});
		contentArea = findViewById(R.id.link_handler_content);

		Intent intent = getIntent();
		if (intent.getAction().equals(Intent.ACTION_VIEW)){
			urlArea.setText(intent.getData().toString());
		} else if (intent.getAction().equals(Intent.ACTION_SEND)){
			String text = "";
			if(intent.hasExtra(Intent.EXTRA_SUBJECT)){
				text += intent.getStringExtra(Intent.EXTRA_SUBJECT) +"\n";
			}
			if(intent.hasExtra(Intent.EXTRA_TEXT)){
				text += intent.getStringExtra(Intent.EXTRA_TEXT);
			}
			urlArea.setText(text);
		}

		actionOpen.performClick();
	}

	protected void onDestroy() {
		if( actionSave.isChecked() ){
			HistoryHelper historyHelper = new HistoryHelper(this);
			historyHelper.insert(urlArea.getText().toString());
			historyHelper.close();
		}

		super.onDestroy();
	}

	private void listHandlers(String target) {
		currentAction = target;

		PackageManager pm = getPackageManager();

		Intent intent = new Intent();
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		if (target.equals("open")) {
			intent.setAction(Intent.ACTION_VIEW);
			intent.setData(Uri.parse(urlArea.getText().toString()));
		} else if (target.equals("share")) {
			intent.setAction(Intent.ACTION_SEND);
			intent.setType("text/plain");
			intent.putExtra(Intent.EXTRA_TEXT, urlArea.getText().toString());
		}

		List<ResolveInfo> resolves = pm.queryIntentActivities(intent, PackageManager.MATCH_ALL | PackageManager.MATCH_DISABLED_UNTIL_USED_COMPONENTS);

		final Collator collator = Collator.getInstance(Locale.getDefault());
		Collections.sort(resolves, (o1, o2) -> {
			return collator.compare(
			 o1.activityInfo.loadLabel(pm).toString().toLowerCase(Locale.getDefault()),
			 o2.activityInfo.loadLabel(pm).toString().toLowerCase(Locale.getDefault())
			);
		});

		contentArea.removeAllViews();
		List<HandlerEntry> handlerEntries = new ArrayList<>();
		for (ResolveInfo resolve_info : resolves) {
			if (resolve_info.activityInfo.packageName.equals(getPackageName())) {
				continue;
			}

			handlerEntries.add(new HandlerEntry(this)
					.setIcon(resolve_info.loadIcon(pm))
					.setLabel(resolve_info.loadLabel(pm).toString())
					.setComponent(ComponentName.createRelative(resolve_info.activityInfo.packageName, resolve_info.activityInfo.name).flattenToShortString())
					.setIntent(intent));
		}

		// filter and order (again)
		if (target.equals("open")) {
			if (!ruleBrowserLabelOnlyAllowSet.isEmpty()) {
				handlerEntries.removeIf(e -> !ruleBrowserLabelOnlyAllowSet.contains(e.label.getText().toString()));
			}
			handlerEntries.removeIf(e -> ruleBrowserLabelToHideSet.contains(e.label.getText().toString()));
			handlerEntries.sort((a, b) -> Integer.compare(ruleBrowserLabelOrderMap.getOrDefault(a.label.getText().toString(), 0), ruleBrowserLabelOrderMap.getOrDefault(b.label.getText().toString(), 0)));
		} else if (target.equals("share")) {
			if (!ruleShareLabelOnlyAllowSet.isEmpty()) {
				handlerEntries.removeIf(e -> !ruleShareLabelOnlyAllowSet.contains(e.label.getText().toString()));
			}
			handlerEntries.removeIf(e -> ruleShareLabelToHideSet.contains(e.label.getText().toString()));
			handlerEntries.sort((a, b) -> Integer.compare(ruleShareLabelOrderMap.getOrDefault(a.label.getText().toString(), 0), ruleShareLabelOrderMap.getOrDefault(b.label.getText().toString(), 0)));
		}

		handlerEntries.forEach(contentArea::addView);
	}

	class HandlerEntry extends LinearLayout {

		private String component;
		private Intent intent;
		private ImageView icon;
		private TextView label;

		public HandlerEntry(Context context) {
			super(context);

			setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, itemHeight));
			setOrientation(LinearLayout.HORIZONTAL);
			setOnClickListener((v) -> {
				if (intent != null && component != null) {
					intent.setComponent(ComponentName.unflattenFromString(component));
					try {
						startActivity(intent);
//						finish();
						 finishAndRemoveTask();
					} catch (Exception e) {
						Toast.makeText(getContext(), getString(R.string.link_handler_error_launch_failed), Toast.LENGTH_LONG).show();
					}
				}
			});

			icon = new ImageView(context);
			icon.setLayoutParams(new LayoutParams((int)((double)itemHeight * 1.2), itemHeight));
			icon.setPadding((int)((double)itemHeight * 0.15), (int)((double)itemHeight * 0.1), (int)((double)itemHeight * 0.15), (int)((double)itemHeight * 0.1));
			icon.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
			addView(icon);

			label = new TextView(context);
			label.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
			label.setGravity(Gravity.START | Gravity.CENTER_VERTICAL);
			label.setText("");
			label.setTypeface(Typeface.MONOSPACE);
			label.setTextSize(TypedValue.COMPLEX_UNIT_DIP, (int)((double)itemHeight * 0.1));
			label.setTextColor(getColor(R.color.foreground_primary));
			addView(label);
		}

		public HandlerEntry setIcon(Drawable icon) {
			this.icon.setImageDrawable(icon);
			return HandlerEntry.this;
		}

		public HandlerEntry setLabel(String label) {
			this.label.setText(label);
			return HandlerEntry.this;
		}

		public HandlerEntry setComponent(String component) {
			this.component = component;
			return HandlerEntry.this;
		}

		public HandlerEntry setIntent(Intent intent) {
			this.intent = intent;
			return HandlerEntry.this;
		}
	}
}
